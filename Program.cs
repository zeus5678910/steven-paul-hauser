using System;

namespace Fibonacci_Generator
{
    class Program
    {

        static  int Fibonacci_Iterative(int len)
        {
            int a = 0, b = 1, c = 0;
            for (int i = 2; i < len; i++)
            {
                c = a + b;
                a = b;
                b = c;
            }
            return c;
        }
        static void Main(string[] args)
        {

            int FibNumberResult = 0;


            Console.WriteLine("-----------------");
            int FinalValue = 4000000;   // Max value for loop.
            bool KeepRunning = true;    // Boolean to control while loop.
            int counter = 1;            // Counter for the while loop to calculate the Fibonacci number.
            int FibEvenTotal = 0;       // Fibonacci total for even numbers.
            while (KeepRunning)
            {
                // return the Fib Number
                FibNumberResult = Fibonacci_Iterative(counter);

                // Is it > 4,000,000 ?  - then exit

                if (FibNumberResult < FinalValue)
                {
                    // check for fibnumber even or odd
                    if (FibNumberResult % 2 == 0)
                    {
                        // Add the even fibonacci number to the total.
                        FibEvenTotal = FibEvenTotal + FibNumberResult;
                        Console.WriteLine("{0}  EVEN", FibNumberResult);
                    }
                    else
                    {
                        Console.WriteLine(FibNumberResult);
                    }
                }
                else 
                {
                    KeepRunning = false; // Stop the loop if fibonacci number greater than 4,000,000
                }
                counter++;

            }
            Console.WriteLine (" The Total Even Fibonacci Numbers Less Than 4,000,000 = {0}", FibEvenTotal );        
        }
    }
}
