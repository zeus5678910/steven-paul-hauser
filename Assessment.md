## Development Experience

### Technologies used recently

* C#
* ASP.NET MVC
* Javascript
* NodeJS
* JQuery 
* SQL Server 2016
* Visual Studio 2019

### Favorite Language 	: C#
### Favorite Tool 		: Visual Studio


## Best Advice
* Keep learning, keep trying new things. 
